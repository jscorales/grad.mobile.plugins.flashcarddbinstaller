/**
 *
 * FlashcardDbInstaller.java 
 *
 * Created by Junel Corales on 11/26/13.
 *
 */
package com.kaptest.mobile.plugins;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import android.os.Bundle;
import android.util.Log;
import android.content.Context;

import org.apache.cordova.*;

public class FlashcardDbInstaller extends CordovaPlugin
{
	/**
	* Constructor
	*/
	public FlashcardDbInstaller()
	{
	}

	/**
     * Executes the request and returns PluginResult.
     *
     * @param action            The action to execute.
     * @param args              JSONArry of arguments for the plugin.
     * @param callbackContext   The callback id used when calling back into JavaScript.
     * @return                  True if the action was valid, false if not.
     */
    public boolean execute(String action, JSONArray args, CallbackContext callbackContext) throws JSONException {
    	boolean status = true;

        if (action.equals("install")) {
            JSONObject o = args.getJSONObject(0);
            String dbpath = o.getString("path");
			String dbname = o.getString("name");

			status = this.installDatabase(dbpath, dbname);
			if (status)
				callbackContext.success();
			else
				callbackContext.error("");

        }
        else {
            status = false;
        }

        return status;
    }

    /**
	 * Installs an existing database.
	 *
	 * @param dbpath	The path of the database.	
	 * @param dbname	The name of the database to be installed.
	 * @return 			True if the database is existing or was installed successfully.
	 */
	private boolean installDatabase(String dbpath, String dbname)
	{
		try
		{
			Context context = this.cordova.getActivity().getApplicationContext();
			String appPackageName = context.getPackageName();
	      	String databaseFilePath = "/data/data/" + appPackageName + "/databases/" + dbname;
	      	String sourceDbPath = this.ensurePath(dbpath);
	      	
	      	if (fileExists(databaseFilePath))
	      	{
	        	//Log.d("FlashCards", "Database already exists.");
	        	return true;
			}

     	 	this.copy(sourceDbPath, dbname, "/data/data/" + appPackageName + "/databases/");
     	 	Log.v("FlashCards", "Database was installed successfully.");
    	}
    	catch (IOException e)
    	{
   			Log.v("FlashCards", "Database installation failed.");
      		e.printStackTrace();
      		return false;
    	}

    	return true;
	}
	
	private String ensurePath(String dbpath)
	{
		String path = dbpath;

		if (path.startsWith("/"))
			path = path.substring(1);

		if (!path.endsWith("/"))
			path += "/";

		return path;
	}

	/**
	 * Checks if a file exists on the specified path.
	 *
	 * @param filePath 	The path where to look for a file.
	 * @return 			True if the file exists.
	 */
	private Boolean fileExists(String filePath){
    	File file = new File(filePath);

    	return file.exists();
  	} 

	/**
	* Copies file
	*
	* @param file 		The file to be copied.
	* @param folder		The complete path of the destination directory.
	*/
	private void copy(String sourceFolder, String file, String folder) throws IOException 
	{
		File CheckDirectory;
		CheckDirectory = new File(folder);

		if (!CheckDirectory.exists()){ 
			CheckDirectory.mkdir();
		}

		Context context = this.cordova.getActivity().getApplicationContext();
		InputStream in = context.getAssets().open(sourceFolder + file);
		OutputStream out = new FileOutputStream(folder + file);

		// Transfer bytes from in to out
		byte[] buf = new byte[1024];
		int len; while ((len = in.read(buf)) > 0) out.write(buf, 0, len);
		in.close(); out.close();
	}
}