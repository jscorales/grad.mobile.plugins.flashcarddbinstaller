//
//  FlashcardDbInstaller.m
//  Flashcards
//
//  Created by Junel Corales on 11/25/13.
//
//

#import "FlashcardDbInstaller.h"

@implementation FlashcardDbInstaller

-(NSString*)dbPath:(NSString*)dbName
{
	NSString *documentDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
	return [documentDir stringByAppendingPathComponent:dbName];
}

-(void) install:(CDVInvokedUrlCommand *)command
{
	CDVPluginResult* pluginResult = nil;
    NSMutableDictionary *options = [command.arguments objectAtIndex:0];

    NSString *dbPath = [options objectForKey:@"path"];
	NSString *dbName = [options objectForKey:@"name"];
	NSString *dbSourcePath = [dbPath stringByAppendingPathComponent:dbName];
	NSString *absDbSourcePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:dbSourcePath];
	
	NSFileManager *fileManager = [NSFileManager defaultManager];
	 
	if (dbSourcePath == NULL)
	{
		NSLog(@"Source database path was not specified.");
        pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"You must specify the database path."];
		
		[self.commandDelegate sendPluginResult:pluginResult callbackId: command.callbackId];
		
		return;
    }
	
	if (dbName == NULL)
	{
		NSLog(@"Source database name was not specified.");
		pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString:@"You must specify the database name."];
		
		[self.commandDelegate sendPluginResult:pluginResult callbackId: command.callbackId];
		
		return;
	}
	
	if (![fileManager fileExistsAtPath: absDbSourcePath])
	{
		NSLog(@"%@ cannot be found.", absDbSourcePath);
		pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: [NSString stringWithFormat:@"%@ cannot be found.", absDbSourcePath]];
	}
	else
	{
		//get the db destination dir
		NSString *dbDestinationPath = [self dbPath:dbName];
		if ([fileManager fileExistsAtPath: dbDestinationPath])
		{
			//NSLog(@"Database %@ already exists.", dbName);
			pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: [NSString stringWithFormat:@"Database %@ already exists.", dbName]];
		}
		else
		{
			NSError *error = nil;

			// install it by copying it
			[fileManager copyItemAtPath:absDbSourcePath toPath:dbDestinationPath error:&error];
			if (error == nil)
			{
				//prevent iOS from creating an iCloud backup of the database
				NSError *error = nil;
				NSURL *dbUrl = [NSURL fileURLWithPath:dbDestinationPath];
				BOOL success = [dbUrl setResourceValue: [NSNumber numberWithBool: YES] forKey: NSURLIsExcludedFromBackupKey error: &error];
				if(!success){
					NSLog(@"Error excluding %@ from iCloud backup %@", [dbUrl lastPathComponent], error);
				}
				
				NSLog(@"Database %@ was installed successfully.", dbName);
				pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsString: [NSString stringWithFormat:@"Database %@ was installed successfully.", dbName]];
			}
			else
			{
				NSLog(@"Database installation failed.\nReason: %@", [error description]);
				pluginResult = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsString: [NSString stringWithFormat:@"An error occured while installing %@.", dbName]];
			}
		}
	}
	
	
	[self.commandDelegate sendPluginResult:pluginResult callbackId: command.callbackId];
}

@end
