//
//  FlashcardDbInstaller.h
//  Flashcards
//
//  Created by Junel Corales on 11/25/13.
//
//

#import <Foundation/Foundation.h>
#import <Cordova/CDVPlugin.h>

@interface FlashcardDbInstaller : CDVPlugin
-(void) install: (CDVInvokedUrlCommand*)command;
@end
