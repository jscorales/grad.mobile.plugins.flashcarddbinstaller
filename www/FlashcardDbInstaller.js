/**
 *
 * FlashcardDbInstaller.java 
 *
 * Created by Junel Corales on 11/26/13.
 *
 */
module.exports = {
	install: function(installArgs, installSuccess, installError){
		//check for db path and db name
		if (!(installArgs && installArgs['path']))
			throw new Error("Database path argument is missing.");

		if (!(installArgs && installArgs['name']))
			throw new Error("Cannot install database without a name.");

		cordova.exec(installSuccess, installError, "FlashcardDbInstaller", "install", [installArgs]);
	}
};